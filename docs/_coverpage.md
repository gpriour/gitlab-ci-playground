# Previsite Documentation <small>v0.0.1</small>

> A magical documentation site generator.

- Simple and lightweight (~21kB gzipped)
- No statically built html files
- Multiple themes

[GitHub](https://github.com/docsifyjs/docsify/)
[Get Started](#docsify)

<!-- background color -->
![color](#f0f0f0)
